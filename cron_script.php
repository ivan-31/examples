<?php
/**
 * Пример cron-скрипта для CRM "Слушатель" (дистанционное образование)
 * Задача: периодическая проверка базы слушателей на предмет заполненности поля СНИЛС в личном деле. Информация о слушателях с неуказанным СНИЛС однократно отсылается по эмейл соответствующим менеджерам учебных центров.
 * (Используются настройки и доп. скрипты CRM, PHP 5.3, БД MySQL 5.)
 * 
 */

$script_name = $_SERVER['SCRIPT_FILENAME'];
$cron_pos = strpos($script_name, '/cron/');
$PROJECT_ROOT = substr($script_name, 0, $cron_pos);

require_once($PROJECT_ROOT.'/db.php');
require_once($PROJECT_ROOT.'/config.php');
require_once($PROJECT_ROOT.'/functions.php');

require_once($PROJECT_ROOT."/module/lib/phpmailer/class.phpmailer.php");

$crm_arr = array();
$manager_arr = array();
$client_arr = array();
$status_ids = array();
$email_subject = "Список слушателей с незаполненным полем СНИЛС на ".date('d.m.Y');

// Выбираем данные менеджеров с правом получать отчеты по СНИЛС, заодно формируем список их УЦ, которые нужно  обойти
$manager_result = mysql_query("SELECT manager_id, crm_id, manager_email, special_access FROM docs_manager WHERE special_access LIKE '%may_receive_snils_report%' AND manager_email !='' AND manager_active=1");
if (mysql_num_rows($manager_result)) {
    while (list($manager_id, $crm_id, $manager_email, $special_access) = mysql_fetch_row($manager_result)) {
        $special_access = unserialize($special_access);
        if (is_array($special_access) && !empty($special_access['may_receive_snils_report'])) {
            if (isset($manager_arr[$manager_email])) {
                if (!in_array($crm_id, $manager_arr[$manager_email])) {
                    $manager_arr[$manager_email][] = $crm_id;
                }
            } else {
                $manager_arr[$manager_email][] = $crm_id;
            }
            if (!in_array($crm_id, $crm_arr)) {
                $crm_arr[$crm_id] = array();
            }
        }
    }
}

if (!empty($crm_arr)) { // массив пуст, если менеджеров с правами на отчет не нашлось
    // Выбираем данные по УЦ (для указания отправителя эмейл)   
    $crm_result = mysql_query("SELECT crm_id, email_from_name, email_from, unisender_list_id, legal_name_short AS crm_legal_name_short, mandrill_api_active FROM docs_crm WHERE crm_login NOT LIKE 'prototype\_%' AND crm_active=1 AND crm_id IN(".implode(',', array_keys($crm_arr)).")");
    if (mysql_num_rows($crm_result)) {
        while ($row = mysql_fetch_assoc($crm_result)) {
            $crm_arr[$row['crm_id']] = $row;
        }
    }
    
    // Получаем массив ID статусов для status_code='audit_inspector' (с учетом разных типов обучения, на будущее)
    $status_ids_result = mysql_query("SELECT status_id FROM docs_status WHERE status_code='audit_inspector' AND status_active=1");
        if (mysql_num_rows($status_ids_result)) {
            while (list($status_id) = mysql_fetch_row($status_ids_result)) {
               $status_ids[] = $status_id; 
            }
        } else $status_ids = array(0);
    
    // Собираем данные по слушателям с незаполненным СНИЛС (условия: гражданство РФ, в последний месяц проставлен аудит, отличный от "", получает диплом или удостоверение, поле "СНИЛС" в карточке не заполнено)
    $client_result = mysql_query("SELECT DISTINCT t1.crm_id, t1.client_surname, t1.client_name, t1.father_name 
FROM docs_client t1 
JOIN docs_product t2 ON (IF(t1.product_id_2 !=0, t1.product_id_2, t1.product_id)=t2.product_id) 
JOIN docs_client_status t3 ON (t1.client_id=t3.client_id AND t3.status_id IN(".implode(',', $status_ids).") AND t3.status_value !='') 
WHERE t1.crm_id IN(".implode(',', array_keys($crm_arr)).") AND t1.snils='' AND t1.citizenship='Российская Федерация' AND t2.product_code IN('ПП', 'КПП', 'ПК') 
AND t1.special_graduation_document !='свидетельство'
AND t3.status_date BETWEEN CURDATE() - INTERVAL 1 MONTH AND CURDATE()
ORDER BY t1.client_surname, t1.client_name, t1.father_name");
    if (mysql_num_rows($client_result)) {
        while (list($crm_id, $client_surname, $client_name, $father_name) = mysql_fetch_row($client_result)) {
            $client_arr[$crm_id][] = "$client_surname $client_name $father_name";
        }
    }

    // Сравниваем текущий массив слушателей с ранее сохраненным, оставляем только новых
    if (!empty($client_arr)) {
        foreach ($client_arr as $crm_id => $fio_arr) {
            if (file_exists($PROJECT_ROOT."/cron/signal/no_snils/no_snils_{$crm_id}.txt")) {
                $prev_fio_arr = file($PROJECT_ROOT."/cron/signal/no_snils/no_snils_{$crm_id}.txt", FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES);
                $client_arr[$crm_id] = array_diff($fio_arr, $prev_fio_arr);
                if (empty($client_arr[$crm_id])) unset($client_arr[$crm_id]);
            }
        }
    }
    
    // Если есть новые слушатели, формируем отчет
    if (!empty($client_arr)) {
        foreach ($manager_arr as $manager_email => $crm_arr_2) {
            $email_text = '';
            foreach ($crm_arr_2 as $crm_id) {
                $client_list = '';
                if (!empty($client_arr[$crm_id])) { 
                    // данные УЦ-отправителя (в итоге, письмо будет отправлено от имени последнего разобранного в цикле)
                    foreach ($crm_arr[$crm_id] as $key => $val) ${$key} = $val;
                    
                    $email_text .= "<p><u>$crm_legal_name_short</u></p><ol>";
                    
                    foreach($client_arr[$crm_id] as $client_fio) {
                        $client_list .= "<li>$client_fio</li>";
                    }
                    
                    $email_text .= "$client_list</ol>";
                    
                    if (empty($email_from) || empty($unisender_list_id)) continue;                    
                    $from = $email_from_name ? "$email_from_name<$email_from>" : $email_from; 
                }
            }
            // Отправляем отчет менеджеру (если в одном УЦ права на отчет у нескольких менеджеров, то тот же список слушателей будет отправлен каждому)
            if (!empty($email_text) && !empty($from)) {
                $email_text = "<h3>$email_subject</h3>$email_text";
                mail_universal($from, $manager_email, $email_subject, $email_text);
            }
        }
        // Добавляем новых слушателей в лог (чтобы второй раз о них не сообщать)
        foreach ($client_arr as $crm_id => $crm_client_arr) {
            file_put_contents($PROJECT_ROOT."/cron/signal/no_snils/no_snils_{$crm_id}.txt", "\r\n".implode("\r\n",$crm_client_arr), FILE_APPEND);
        }
    }        
}
?>