<?php
/**
 * Абстрактный класс (php 7.0), который позволяет своим наследникам с помощью полиморфизма реализовать доступ к разным БД
 * (один интерфейс - множество реализаций)
 */

abstract class DB
{
    abstract public function getInfoByFrid(array $arg_arr, $limitoutput);
    abstract public function getInfoByFridWO(array $arg_arr, $limitoutput);
    abstract public function getFridById(array $id_arr);
    abstract public function getUsersByFrid(array $frid_arr);
    abstract public function getUsersByFrid_A(array $frid_arr);
    abstract public function getParfsByUID(array $arr);
    abstract public function liveSearch($arg_str, $st);
    
    abstract public function getOutAccords(array $ac_arr, array $genders_arr, $minaccnum);
    abstract public function getOutNotes(array $notes_arr, array $genders_arr, $minnotesnum);
    abstract public function getInPerf(array $arg_arr);
    abstract public function applyFilters(array $frid_rel_arr, array $segment_arr, array $launchtime_arr);
    abstract public function getOutputsForces(array $frid_rel_arr, $min_total_voted);
    
    protected function argGen(array $arg) 
    {
        $arr = array();
        for($i = 1, $size = count($arg); $i <= $size; ++$i) {
            $arr[] = '$'.$i;
        }
        $str = implode(',', $arr);
        return $str;
    }
}
