<?php
/**
 * Реализация абстрактного класса DB для PostgreSQL 9.5, PHP 7.0
 */

require_once 'DB.class.php';

class DBpostgres extends DB 
{
	private $resource;
	
	function __construct($connection_data) 
	{
		$this->resource = pg_connect($connection_data) or die(); 
	}

	private function query($q, $arr_params, $r='')
	{
		if ($r == '') {
			$r = $this->resource;
		}

		$result = pg_query_params($r, $q, $arr_params) or die();
		return $result;
	}

	private function querySimple ($q, $r='')
	{
		if ($r == '') {
			$r = $this->resource;
		}

		$result = pg_query($r, $q) or die();
		return $result;
	}

	private function qresult($result, $type='default')
	{
		$val_arr = array();

		switch ($type) {
			case 'default':
				while ($val = pg_fetch_assoc($result)) {
					$val_arr[] = $val;
				}
			break;
			case 'allarr': 
				$val_arr = pg_fetch_all($result);
			break;
		}

		return $val_arr;
	}

	public function getInfoByFrid(array $arg_arr, $limitoutput=0)
	{ 
		if ($limitoutput !== 0) {
			$lim = "LIMIT ".$limitoutput;
		} else {
			$lim = '';
		}
	
		$arg_str = parent::argGen($arg_arr);
		$q = "SELECT parf_basic.frid, parf_basic.brand, parf_basic.name, parf_basic.gender, parf_basic.release_year, parf_basic.image, parf_basic.brand||' '||parf_basic.name AS brandname, parf_basic.id FROM parf_basic WHERE parf_basic.frid IN ($arg_str) $lim";
		$res = $this->query($q, $arg_arr);
		return $this->qresult($res);

	}

	public function getInfoByFridWO(array $arg_arr, $limitoutput=0)
	{ 
		if ($limitoutput !== 0) {
			$lim = "LIMIT ".$limitoutput;
		} else {
			$lim = '';
		}

		$arg_str = implode(',', $arg_arr);

		$q = "SELECT frid, brand, name, gender, release_year, image, brand||' '||name AS brandname, id FROM unnest('{ $arg_str }'::int[]) WITH ORDINALITY o(num,ord) JOIN parf_basic ON parf_basic.frid=o.num ORDER BY o.ord $lim";

		$res = $this->querySimple($q); 
		return $this->qresult($res);
	}

	public function getFridById(array $id_arr)
	{
		$arg_str = parent::argGen($id_arr);
		$q = "SELECT parf_basic.frid FROM parf_basic WHERE parf_basic.id IN ($arg_str)";
		$res = $this->query($q, $id_arr);
		return $this->qresult($res);
	}

	public function getUsersByFrid(array $frid_arr) 
	{
		$arg_str = parent::argGen($frid_arr);
		$q = "SELECT wardrobe_parfs.frid, wardrobe_parfs.user_love, wardrobe_parfs.user_like, wardrobe_parfs.user_dislike FROM wardrobe_parfs WHERE wardrobe_parfs.frid IN ($arg_str)";
		$res = $this->query($q, $frid_arr);
		return $this->qresult($res);
	}

	public function getUsersByFrid_A(array $frid_arr) 
	{ 

		$arg_str = parent::argGen($frid_arr);
		$q = "SELECT wardrobe_parfs.frid, string_to_array(wardrobe_parfs.user_love, wardrobe_parfs.user_like, wardrobe_parfs.user_dislike) FROM wardrobe_parfs WHERE wardrobe_parfs.frid IN ($arg_str)";
		$res = $this->query($q, $frid_arr);
		return $this->qresult($res);

	}

	public function getParfsByUID(array $arr) 
	{ 
		$arg_str = parent::argGen($arr);
		$q = "SELECT wardrobe_users.user_id, wardrobe_users.user_love, wardrobe_users.user_like, wardrobe_users.user_dislike FROM wardrobe_users WHERE wardrobe_users.user_id IN ($arg_str)";
		$res = $this->query($q, $arr);
		return $this->qresult($res);
	}

	public function liveSearch($arg_str, $st)
	{
		$arg_str = $this->prepareSearchPhrase($arg_str); 

		if ($st == 2) {
			$searchtypedop = "";
		} else {
			$searchtypedop = " AND tastes=TRUE";
		}
		
		$arr = array("$arg_str"); 
		$q = "SELECT parf_basic.brand, parf_basic.name, parf_basic.brand||' '||parf_basic.name AS brandname, parf_basic.gender, parf_basic.release_year, parf_basic.id, parf_basic.image FROM parf_basic WHERE parf_basic.fullname @@ to_tsquery($1)".$searchtypedop." ORDER BY brandname ASC LIMIT 20"; 
		$res = $this->query($q, $arr);
		return $this->qresult($res);
	}

	private function prepareSearchPhrase ($phrase)
	{
		$input = addcslashes($phrase, '\'"&:;*#$@!?`~,.=+()[]{}|/\\<>^'); 
		if (preg_match('$\s+$', $input) ===1) {

			$input = preg_replace('$\s+$', ':*&', $input, -1, $n);

			if($n>15) {
				return ''; 
			}
		}
		
		return $input.':*';
	}

	function __destruct()
	{
		pg_close($this->resource);
	}

	/* =========================== 2 aromabot ============================= */

	public function getOutAccords(array $ac_arr, array $genders_arr, $minaccnum=ACCMINNUM)
	{	
		$inner_arr = array();
		$arg_str = parent::argGen($ac_arr);

		foreach ($genders_arr as $k => $v) {
			$genders_arr[$k] = "'$v'";
		}

		$genders_str = strtoupper(implode(',', $genders_arr));

		$q = "SELECT a.frid, a.accord_id, a.abs, a.percent, a.gender FROM parf_accords as a WHERE a.frid IN (SELECT aa.frid FROM parf_accords as aa WHERE aa.accord_id IN ($arg_str) GROUP BY aa.frid, aa.gender HAVING aa.gender IN($genders_str) AND count(aa.frid) >= $minaccnum) LIMIT 100000";

		$res = $this->query($q, $ac_arr);
		$inner_arr = $this->qresult($res);

		return $this->formatAccOutput($inner_arr);
	}

	public function getOutNotes(array $notes_arr, array $genders_arr, $minnotesnum=NOTEMINNUM)
	{		
		$inner_arr = array();
		$arg_str = parent::argGen($notes_arr);

		foreach ($genders_arr as $k => $v) {
			$genders_arr[$k] = "'$v'";
		}

		$genders_str = strtoupper(implode(',', $genders_arr));

		$q = "SELECT a.frid, a.note_id, a.abs, a.top_percent, a.percent, a.gender FROM parf_mainnotes as a WHERE a.frid IN (SELECT aa.frid FROM parf_mainnotes as aa WHERE aa.note_id IN ($arg_str) GROUP BY aa.frid, aa.gender HAVING aa.gender IN($genders_str) AND count(aa.frid) >= $minnotesnum) LIMIT 300000";

		$res = $this->query($q, $notes_arr);
		$inner_arr = $this->qresult($res);

		return $this->formatNoteOutput($inner_arr);
	}

	private function formatAccOutput (array $result)
	{
		if (!empty($result)) {
			$output_arr = array();
			foreach ($result as $k => $v) {
				if (isset($result[$k]['arr'])) {
					$arr_decode = json_decode($result[$k]['arr'], true);
					foreach ($arr_decode as $k2 => $v2) {
						$output_arr[$v['frid']][$v2[0]] = array('abs' => $v2[1], 'percent' => $v2[2]);
					}
				} else {
					$output_arr[$v['frid']][$v['accord_id']] = array('abs' => $v['abs'], 'percent' => $v['percent']);
				}
				$output_arr[$v['frid']]['gender'] = $v['gender'];
			}
			return $output_arr;
		} else return array();
	}

	private function formatNoteOutput (array $result)
	{
		if (!empty($result)) {
			$output_arr = array();
			foreach ($result as $k => $v) {
				if (isset($result[$k]['arr'])) {
					$arr_decode = json_decode($result[$k]['arr'], true);
					foreach ($arr_decode as $k2 => $v2) {
						$output_arr[$v['frid']][$v2[0]] = array('abs' => $v2[1], 'percent' => $v2[3], 'top_percent' => $v2[2]);
					}
				} else {
					$output_arr[$v['frid']][$v['note_id']] = array('abs' => $v['abs'], 'percent' => $v['percent'], 'top_percent' => $v['top_percent']);
				}
				$output_arr[$v['frid']]['gender'] = $v['gender'];
			}
			return $output_arr;
		} else return array();
	}

	public function getInPerf(array $arg_arr)
	{
		$output_arr = array();	
		$arg_str = parent::argGen($arg_arr);

		$q = "SELECT b.frid, b.brand, b.name, b.gender, b.release_year, b.segment, b.id, p.total_voted, f.day_night_medium, f.day_night, f.longevity_medium, f.seasons_medium, f.sillage_medium, array_to_json(array_agg(DISTINCT ARRAY[s.one, s.two, s.score])) as similar,  array_to_json(array_agg(DISTINCT ARRAY[a.accord_id, a.abs, a.percent])) as accords,array_to_json(array_agg(DISTINCT ARRAY[n.note_id, n.abs, n.top_percent, n.percent])) as notes FROM parf_basic AS b LEFT JOIN parf_forces AS f ON f.frid=b.frid LEFT JOIN parf_similar_com AS s ON s.one=b.frid OR s.two=b.frid LEFT JOIN parf_accords AS a ON a.frid=b.frid LEFT JOIN parf_mainnotes AS n ON n.frid=b.frid LEFT JOIN parf_popularity_com AS p ON p.frid=b.frid WHERE b.id IN ($arg_str) GROUP BY b.frid, f.day_night_medium, f.day_night, f.longevity_medium, f.seasons_medium, f.sillage_medium, p.total_voted";

		$res = $this->query($q, $arg_arr);
		$out = $this->qresult($res);

		$output_arr = array();

		if (!empty($out)) {
			foreach ($out as $k => $v) {
				if ($v['similar'] != '[[null,null,null]]') {
					$v['similar'] = json_decode($v['similar'], true);
					foreach ($v['similar'] as $k1 => $v1) {
						$v['similar'][$k1] = array('one'=>$v1[0], 'two'=>$v1[1], 'score'=>$v1[2]);
					}
				} else {
					$v['similar'] = array();
				}

				if ($v['accords'] != '[[null,null,null]]') {
					$accords_newarr = json_decode($v['accords'], true);
					$v['accords'] = array();
					foreach ($accords_newarr as $k2 => $v2) {
						$v['accords'][$v2[0]] = array('abs' => $v2[1], 'percent' => $v2[2]);
					}
				} else {
					$v['accords'] = array();
				}

				if ($v['notes'] != '[[null,null,null,null]]') {
					$notes_newarr = json_decode($v['notes'], true);
					$v['notes'] = array();
					foreach ($notes_newarr as $k3 => $v3) {
						$v['notes'][$v3[0]] = array('abs' => $v3[1], 'top_percent' => $v3[2], 'percent' => $v3[3]);
					}
				} else {
					$v['notes'] = array();
				}

				if ($v['day_night'] == '0,0') {
					$v['day_night_medium'] = '';
				}

				unset($v['day_night']);

				$output_arr[$v['frid']] = $v;
			}
		}

		return $output_arr;
	}

	public function applyFilters(array $frid_rel_arr, array $segment_arr, array $launchtime_arr)
	{
		$arg_arr = array_keys($frid_rel_arr);
		$arg_str = parent::argGen($arg_arr);

		$filters = '';

		if (!empty($segment_arr)) {
			$filters .= ' AND segment IN('.implode(',', $segment_arr).')';
		}

		if (!empty($launchtime_arr)) {
			$filters .= ' AND release_year BETWEEN '.$launchtime_arr['start'].' AND '.$launchtime_arr['end'];
		}

		$q = "SELECT frid FROM parf_basic WHERE frid IN ($arg_str) $filters";

		$res = $this->query($q, $arg_arr);
		$out = $this->qresult($res);

		$out_format = array();
		foreach ($out as $row) {
			$out_format[] = $row['frid'];
		}

		foreach ($frid_rel_arr as $frid => $rel) {
			if (!in_array($frid, $out_format)) {
				unset($frid_rel_arr[$frid]);
			}
		}

		return $frid_rel_arr;
	}

	public function getOutputsForces(array $frid_rel_arr, $min_total_voted)
	{
		$output_arr = array(); 

		$arg_arr = array_keys($frid_rel_arr);
		$arg_str = parent::argGen($arg_arr);
		$min_total_voted = (int) $min_total_voted;

		$q = "SELECT f.frid, p.total_voted, f.seasons_medium, f.day_night_medium, f.day_night FROM parf_forces AS f JOIN parf_popularity_com AS p ON f.frid=p.frid WHERE p.total_voted >= $min_total_voted AND f.frid IN ($arg_str)";

		$res = $this->query($q, $arg_arr);
		$out = $this->qresult($res);

		foreach ($out as $arr) {
			$output_arr[$arr['frid']]['total_voted'] = $arr['total_voted'];

			if ($arr['seasons_medium'] !== 0) {
				$output_arr[$arr['frid']]['seasons_medium'] = $arr['seasons_medium'];
			}

			if ($arr['day_night'] != '0,0') {
				$output_arr[$arr['frid']]['day_night_medium'] = $arr['day_night_medium'];
			}
		}
		return $output_arr;
	}
}
